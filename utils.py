def round_to_two_decimal_places(number_to_round):
    return float(format(number_to_round, '.2f'))