import os

CONTENT_TYPE_JSON = 'application/json'

MIN_AMORTIZATION_PERIOD = 5
MAX_AMORTIZATION_PERIOD = 25

FIRST_DOWN_PAYMENT_THRESHOLD = 500000
MAX_INSURABLE_AMOUNT = 1000000

FIRST_DOWN_PAYMENT_THRESHOLD_PERCENTAGE = 0.05
SECOND_DOWN_PAYMENT_THRESHOLD_PERCENTAGE = 0.10
MAX_INSURABLE_DOWN_PAYMENT_PERCENTAGE = 0.20

INSURANCE_VALUES = [(0.05, 0.10, 0.0315), (0.10, 0.15, 0.024), (0.15, 0.20, 0.018), (0.20, 1, 0)]

DEFAULT_INTEREST_RATE = 0.025

OLD_INTEREST_RATE_KEY = "old_interest_rate"
NEW_INTEREST_RATE_KEY = "new_interest_rate"
MORTGAGE_PRINCIPAL_AMOUNT_KEY = "mortgage_principal_amount"


BASEDIR = os.path.abspath(os.path.dirname(__file__))


class BaseConstants(object):
    pass


class ParameterNames(BaseConstants):
    AMORTIZATION_PERIOD = 'amortization_period'
    ASKING_PRICE = 'asking_price'
    DOWN_PAYMENT = 'down_payment'
    INTEREST_RATE = 'interest_rate'
    LOAN_PRINCIPAL = 'loan_principal'
    PAYMENT_AMOUNT = 'payment_amount'
    PAYMENT_SCHEDULE = 'payment_schedule'


class HttpResponseCodes(BaseConstants):
    HTTP_OK = 200
    HTTP_BAD_REQUEST = 400


class PaymentScheduleNames(BaseConstants):
    BIWEEKLY = "biweekly"
    WEEKLY = "weekly"
    MONTHLY = "monthly"


MANDATORY_PAYMENT_AMOUNT_ARGS = { ParameterNames.ASKING_PRICE: int,
                                  ParameterNames.DOWN_PAYMENT: float,
                                  ParameterNames.PAYMENT_SCHEDULE: str,
                                  ParameterNames.AMORTIZATION_PERIOD: int}
MANDATORY_MORTGAGE_PRINCIPAL_ARGS = {ParameterNames.PAYMENT_AMOUNT: float,
                                     ParameterNames.PAYMENT_SCHEDULE: str,
                                     ParameterNames.AMORTIZATION_PERIOD: int}

NUM_PAYMENTS_BY_SCHEDULE = {PaymentScheduleNames.WEEKLY: 52,
                            PaymentScheduleNames.BIWEEKLY: 26,
                            PaymentScheduleNames.MONTHLY: 12}
