class BaseBadRequest(Exception):
    def __init__(self):
        Exception.__init__(self)
        self.status_code = 400


class InvalidArguments(BaseBadRequest):
    def __init__(self, message):
        super(InvalidArguments, self).__init__()
        self.message = "Invalid Argument: %s" % (message,)


class MissingArguments(BaseBadRequest):
    def __init__(self, message):
        super(MissingArguments, self).__init__()
        self.message = "Missing Arguments(s): %s" % (message,)


class MissingRequestBody(BaseBadRequest):
    def __init__(self, message):
        super(MissingRequestBody, self).__init__()
        self.message = "The request is missing a body for %s" % (message,)
