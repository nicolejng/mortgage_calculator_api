import unittest
from app.validator import *
from constants import MANDATORY_MORTGAGE_PRINCIPAL_ARGS, MANDATORY_PAYMENT_AMOUNT_ARGS, NUM_PAYMENTS_BY_SCHEDULE, \
                      PaymentScheduleNames


class TestCase(unittest.TestCase):
    # get_argument_dictionary tests
    def test_get_empty_argument_dictionary(self):
        self.assertRaises(MissingArguments, get_argument_dictionary, {}, MANDATORY_MORTGAGE_PRINCIPAL_ARGS)

    def test_get_missing_argument_dictionary(self):
        request_args = {ParameterNames.ASKING_PRICE: "10000000"}
        self.assertRaises(MissingArguments, get_argument_dictionary, request_args, MANDATORY_PAYMENT_AMOUNT_ARGS)

    def test_get_extra_argument_dictionary(self):
        request_args = {ParameterNames.PAYMENT_AMOUNT: "1000.00",
                        ParameterNames.PAYMENT_SCHEDULE: PaymentScheduleNames.WEEKLY,
                        ParameterNames.AMORTIZATION_PERIOD: "6",
                        ParameterNames.ASKING_PRICE: "2000000"}
        expected_result = {ParameterNames.PAYMENT_AMOUNT: 1000.00,
                           ParameterNames.PAYMENT_SCHEDULE: PaymentScheduleNames.WEEKLY,
                           ParameterNames.AMORTIZATION_PERIOD: 6}
        args_dict = get_argument_dictionary(request_args, MANDATORY_MORTGAGE_PRINCIPAL_ARGS)

        self.assertEqual(expected_result, args_dict,
                         "The dictionaries do not match:\nExpected: %s\nActual: %s" % (expected_result, args_dict))

    def test_get_optional_argument_dictionary(self):
        request_args = {ParameterNames.DOWN_PAYMENT: 200}
        optional_dict = {ParameterNames.DOWN_PAYMENT: float}
        args_dict = get_argument_dictionary(request_args, {}, optional_args_dict=optional_dict)

        self.assertEqual(request_args, args_dict,
                         "The dictionaries do not match:\nExpected: %s\nActual: %s" % (request_args, args_dict))

        self.assertRaises(InvalidArguments, get_argument_dictionary, {}, {}, optional_dict)

    # check_valid_argument_values tests
    def test_check_valid_argument_values_asking_price(self):
        self._test_check_valid_amount(ParameterNames.ASKING_PRICE, invalid_values=[0, -100],
                                      valid_values=[1, 100000])

    def test_check_valid_argument_values_payment_amount(self):
        self._test_check_valid_amount(ParameterNames.PAYMENT_AMOUNT, invalid_values=[0.00, -100.0],
                                      valid_values=[0.01, 1000000.00])

    def test_check_valid_argument_values_down_payment(self):
        self._test_check_valid_amount(ParameterNames.DOWN_PAYMENT, invalid_values=[0.00, -100.0],
                                      valid_values=[0.01, 1000000.00])

    def test_check_valid_argument_values_amortization_period(self):
        self._test_check_valid_amount(ParameterNames.AMORTIZATION_PERIOD, invalid_values=[-5, 4, 26],
                                      valid_values=[5, 15, 25])

    def test_check_valid_argument_values_payment_schedule(self):
        self._test_check_valid_amount(ParameterNames.PAYMENT_SCHEDULE, invalid_values=["", "semi-monthly"],
                                      valid_values=NUM_PAYMENTS_BY_SCHEDULE.keys())

    # check_valid_down_payment tests
    def test_check_down_payment_and_asking_price(self):
        self.assertFalse(check_valid_down_payment(100, 0.00),
                         "Down payment of $0.00 is invalid")
        self.assertFalse(check_valid_down_payment(100, 100.00),
                         "Down payment and asking price are equal. This is invalid.")
        self.assertFalse(check_valid_down_payment(100, 101.00),
                         "Down payment cannot be larger than asking price.")

        self.assertFalse(check_valid_down_payment(500000, 24999),
                         "The validation for 5% of asking price is incorrect")
        self.assertTrue(check_valid_down_payment(500000, 25000),
                        "The validation for 5% of first 500K is incorrect")
        self.assertFalse(check_valid_down_payment(1000000, 74999),
                         "The validation for 10% of remaining asking price is incorrect")
        self.assertTrue(check_valid_down_payment(1000000, 75000),
                        "The validation for 10% of remaining asking price is incorrect")

    """
        Helper method to run multiple valid and invalid values
    """
    def _test_check_valid_amount(self, param_name, invalid_values, valid_values):
        for val in invalid_values:
            self.assertRaises(InvalidArguments, check_valid_argument_values, {param_name: val})

        for val in valid_values:
            self.assertIsNone(check_valid_argument_values({param_name: val}),
                              "Incorrectly determined %s value to be invalid: %s" % (param_name, str(val)))

if __name__ == "__main__":
    unittest.main()
