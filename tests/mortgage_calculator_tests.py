import unittest
from app.mortgage_calculator import *
from constants import PaymentScheduleNames


class TestCase(unittest.TestCase):
    # calculate_mortgage_insurance tests
    def test_calculate_mortgage_insurance_over_insurable_amount(self):
        expected_cost = 0
        insurance_cost = calculate_mortgage_insurance(MAX_INSURABLE_AMOUNT + 1, 10000)
        self.assertEqual(expected_cost, insurance_cost, "Expected: %f\nActual: %f" % (expected_cost,
                                                                                      insurance_cost))

    def test_calculate_mortgage_insurance_twenty_percent_down(self):
        expected_cost = 0
        insurance_cost = calculate_mortgage_insurance(MAX_INSURABLE_AMOUNT, 200000)
        self.assertEqual(expected_cost, insurance_cost, "Expected: %f\nActual: %f" % (expected_cost,
                                                                                      insurance_cost))

    def test_calculate_mortgage_insurance_less_than_twenty_percent_down(self):
        expected_cost = 11520.018
        insurance_cost = calculate_mortgage_insurance(800000, 159999)
        self.assertEqual(expected_cost, insurance_cost, "Expected: %f\nActual: %f" % (expected_cost,
                                                                                      insurance_cost))

    # calculate_payment_amount test
    def test_calculate_payment_amount_no_insurance(self):
        expected_amount = 716.61
        payment_amount = calculate_payment_amount_from_asking(MAX_INSURABLE_AMOUNT, 200000, 25,
                                                              PaymentScheduleNames.WEEKLY, 0.0125)
        self.assertEqual(expected_amount, payment_amount, "Expected: %f\nActual: %f" % (expected_amount,
                                                                                        payment_amount))

    def test_calculate_payment_amount_with_insurance(self):
        expected_amount = 6979.04
        payment_amount = calculate_payment_amount_from_asking(800000, 55000, 5, PaymentScheduleNames.BIWEEKLY, 0.0679)
        self.assertEqual(expected_amount, payment_amount, "Expected: %f\nActual: %f" % (expected_amount,
                                                                                        payment_amount))

    def test_calculate_payment_amount_with_no_insurance_min(self):
        expected_amount = 51.30
        payment_amount = calculate_payment_amount_from_asking(800000, 790000, 20, PaymentScheduleNames.MONTHLY, 0.0215)
        self.assertEqual(expected_amount, payment_amount, "Expected: %f\nActual: %f" % (expected_amount,
                                                                                        payment_amount))
    # calculate_loan_principal tests
    def test_calculate_loan_principal_no_insurance(self):
        expected_amount = 800000
        loan_principal = calculate_loan_principal_from_payment(716.61, 200000, 25, PaymentScheduleNames.WEEKLY, 0.0125)
        self.assertAlmostEqual(expected_amount, loan_principal, msg="Expected: %f\nActual: %f" % (expected_amount,
                                                                                                  loan_principal),
                               delta=0.005*52*25)

    def test_calculate_loan_principal_with_insurance(self):
        expected_amount = 768467.50
        loan_principal = calculate_loan_principal_from_payment(6979.04, 55000, 5, PaymentScheduleNames.BIWEEKLY, 0.0679)
        self.assertAlmostEqual(expected_amount, loan_principal, msg="Expected: %f\nActual: %f" % (expected_amount,
                                                                                                  loan_principal),
                               delta=0.005*26*5)

    def test_calculate_loan_principal_with_insurance_min(self):
        expected_amount = 10000
        loan_principal = calculate_loan_principal_from_payment(51.30, 790000, 20, PaymentScheduleNames.MONTHLY, 0.0215)
        self.assertAlmostEqual(expected_amount, loan_principal, msg="Expected: %f\nActual: %f" % (expected_amount,
                                                                                                  loan_principal),
                                                                                                  delta=0.005*12*20)


    # calculate_mortgage_amount tests
    def test_calculate_mortgage_amount_with_twenty_down(self):
        expected_amount = 800000
        mortgage_amount = calculate_mortgage_principal_amount(200000.00, 800000.00)
        self.assertEqual(expected_amount, mortgage_amount, "Expected: %f\nActual: %f" % (expected_amount,
                                                                                         mortgage_amount))

    def test_calculate_mortgage_amount_with_less_than_twenty_down(self):
        expected_amount = 640001.00
        mortgage_amount = calculate_mortgage_principal_amount(159999.00, 651521.018)
        self.assertEqual(expected_amount, mortgage_amount, "Expected: %f\nActual: %f" % (expected_amount,
                                                                                         mortgage_amount))

    def test_calculate_mortgage_amount_with_less_than_fifteen_down(self):
        expected_amount = 745000
        mortgage_amount = calculate_mortgage_principal_amount(55000.00, 768467.50)
        self.assertEqual(expected_amount, mortgage_amount, "Expected: %f\nActual: %f" % (expected_amount,
                                                                                         mortgage_amount))


if __name__ == "__main__":
  unittest.main()
