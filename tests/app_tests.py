import unittest

from flask import json

from constants import HttpResponseCodes, CONTENT_TYPE_JSON, ParameterNames, OLD_INTEREST_RATE_KEY, NEW_INTEREST_RATE_KEY
from app.app import app
from app.db_controller import db, init_interest_rate_if_necessary


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config.from_object('config.TestingConfig')
        self.app = app.test_client()

        db.app = app
        db.init_app(app)
        db.create_all()
        init_interest_rate_if_necessary()


    def tearDown(self):
        db.drop_all()

    def test_root(self):
        resp = self.app.get("/")

        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_OK)
        self.assertEqual(resp.data, "Welcome to Nicole's Mortgage Calculator API.")

    def test_get_payment_amount_from_asking(self):
        resp = self.app.get("/payment-amount?asking_price=300000&down_payment=60000&payment_schedule=monthly&amortization_period=25")
        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_OK)
        self.assertEqual(resp.content_type, CONTENT_TYPE_JSON)

        data = json.loads(resp.data)
        self.assertEqual(data[ParameterNames.PAYMENT_AMOUNT], 1076.68)

    def test_get_mortgage_amount_from_payment(self):
        resp = self.app.get(
            "/mortgage-amount?payment_amount=51.30&down_payment=790000&payment_schedule=monthly&amortization_period=20")
        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_OK)
        self.assertEqual(resp.content_type, CONTENT_TYPE_JSON)

        data = json.loads(resp.data)
        self.assertEqual(data['mortgage_principal_amount'], 9681.02)

    # Interest Rate Tests
    def test_invalid_argument_interest_rate(self):
        resp = self.app.patch('/interest-rate', content_type=CONTENT_TYPE_JSON)
        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_BAD_REQUEST)

        resp = self.app.patch('/interest-rate', data='{}', content_type=CONTENT_TYPE_JSON)
        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_BAD_REQUEST)

        resp = self.app.patch('/interest-rate', data='{"%s": "something_invalid"}' % (ParameterNames.INTEREST_RATE,),
                              content_type=CONTENT_TYPE_JSON)
        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_BAD_REQUEST)

    def test_change_interest_rate(self):
        # Set up initial interest rate
        initial_interest_rate = 0.0125
        new_interest_rate = 0.025
        resp = self.app.patch('/interest-rate', data='{"%s": %f}' % (ParameterNames.INTEREST_RATE,
                                                                     initial_interest_rate),
                              content_type=CONTENT_TYPE_JSON)
        data = json.loads(resp.data)

        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_OK)
        self.assertEqual(resp.content_type, CONTENT_TYPE_JSON)
        self.assertEqual(data[NEW_INTEREST_RATE_KEY], initial_interest_rate)

        # Compare Initial interest rate with new interest_rate
        resp = self.app.patch('/interest-rate', data='{"%s": %f}' % (ParameterNames.INTEREST_RATE,
                                                                                new_interest_rate,),
                              content_type=CONTENT_TYPE_JSON)
        data = json.loads(resp.data)

        self.assertEqual(resp.status_code, HttpResponseCodes.HTTP_OK)
        self.assertEqual(resp.content_type, CONTENT_TYPE_JSON)

        self.assertEqual(data[NEW_INTEREST_RATE_KEY], new_interest_rate)
        self.assertEqual(data[OLD_INTEREST_RATE_KEY], initial_interest_rate)


if __name__ == "__main__":
  unittest.main()