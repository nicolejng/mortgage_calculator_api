from flask_sqlalchemy import SQLAlchemy
from constants import ParameterNames, DEFAULT_INTEREST_RATE

db = SQLAlchemy()

"""
    Model for storing key-value pairs in the database
"""
class PersistentStore(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    key = db.Column(db.String, unique=True)
    value = db.Column(db.String)

    def __init__(self, key, value):
        self.key = key
        self.value = value


"""
    Helper methods for interest_rate
"""
def _get_interest_rate_row():
    return PersistentStore.query.filter_by(key=ParameterNames.INTEREST_RATE).first()


def get_db_interest_rate():
    db_interest_row = _get_interest_rate_row()
    old_interest_rate = float(db_interest_row.value)

    return old_interest_rate


def set_interest_rate(new_interest_rate):
    db_interest_row = _get_interest_rate_row()
    db_interest_row.value = str(float(new_interest_rate))

    db.session.commit()


# Creates a row in persistent_store if the table is empty (due to table being created via db.create_all)
# with a default interest_rate
def init_interest_rate_if_necessary():
    db_interest_row = _get_interest_rate_row()

    if not db_interest_row:
        interest_rate = PersistentStore(ParameterNames.INTEREST_RATE, DEFAULT_INTEREST_RATE)

        db.session.add(interest_rate)
        db.session.commit()