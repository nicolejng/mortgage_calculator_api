from constants import ParameterNames, MIN_AMORTIZATION_PERIOD, MAX_AMORTIZATION_PERIOD, NUM_PAYMENTS_BY_SCHEDULE, \
                      FIRST_DOWN_PAYMENT_THRESHOLD_PERCENTAGE, FIRST_DOWN_PAYMENT_THRESHOLD, \
                      SECOND_DOWN_PAYMENT_THRESHOLD_PERCENTAGE
from errors import InvalidArguments, MissingArguments


"""
    The following methods ensure that the required arguments passed through the request exist as well as the validity
    of the values for a given parameter
"""


def get_argument_dictionary(arguments_dict_to_check, mandatory_args_dict, optional_args_dict=None):
    missing_args = []
    arguments_dict = {}

    # Iterates through dictionary of mandatory arguments to determine if any are missing
    for arg, arg_type in mandatory_args_dict.iteritems():
        if arg not in arguments_dict_to_check:
            missing_args.append(arg)
        else:
            val = arguments_dict_to_check.get(arg)

            try:
                arguments_dict[arg] = arg_type(val)
            except ValueError:
                raise InvalidArguments("%s %s. Expected type: %s" % (arg, val, arg_type))

    if missing_args:
        raise MissingArguments(repr(missing_args))

    # Adds optional arguments to dictionary if they exist in arguments_dict_to_check
    # Otherwise, adds it by finding the default value for a given optional name
    if optional_args_dict:
        for optional_name, arg_type in optional_args_dict.iteritems():
            arguments_dict[optional_name] = arg_type(arguments_dict_to_check.get(optional_name,
                                                                                 get_default_from_arg_name(optional_name)))

    check_valid_argument_values(arguments_dict)

    return arguments_dict


# Validates values of arguments and raises InvalidArguments if one arguments is found to be invalid
# Logs warning if there is an extra argument, but ignores it otherwise
def check_valid_argument_values(arg_dict):
    for arg_name in arg_dict:
        val = arg_dict[arg_name]

        if arg_name in [ParameterNames.ASKING_PRICE, ParameterNames.PAYMENT_AMOUNT, ParameterNames.DOWN_PAYMENT]:
            is_valid = val > 0
        elif arg_name == ParameterNames.AMORTIZATION_PERIOD:
            is_valid = MIN_AMORTIZATION_PERIOD <= val <= MAX_AMORTIZATION_PERIOD
        elif arg_name == ParameterNames.PAYMENT_SCHEDULE:
            is_valid = val.lower() in NUM_PAYMENTS_BY_SCHEDULE
        else:
            app.logger.warning("There is some unknown argument passed incorrectly in mandatory_names: %s" % (arg_name,))
            is_valid = True

        if arg_name == ParameterNames.DOWN_PAYMENT and ParameterNames.ASKING_PRICE in arg_dict:
            is_valid = check_valid_down_payment(arg_dict[ParameterNames.ASKING_PRICE], val)

        if not is_valid:
            raise InvalidArguments("%s has invalid value: %s" % (arg_name, val))


def check_valid_down_payment(asking_price, down_payment):
    if asking_price <= down_payment or down_payment <= 0:
        is_valid = False
    else:
        down_payment_check = min(asking_price, FIRST_DOWN_PAYMENT_THRESHOLD) * FIRST_DOWN_PAYMENT_THRESHOLD_PERCENTAGE

        if asking_price > FIRST_DOWN_PAYMENT_THRESHOLD:
            remaining_asking_price = (asking_price - FIRST_DOWN_PAYMENT_THRESHOLD)
            down_payment_check += (remaining_asking_price * SECOND_DOWN_PAYMENT_THRESHOLD_PERCENTAGE)

        is_valid = down_payment >= down_payment_check

    return is_valid


# This method can be extended for other argument names when necessary
def get_default_from_arg_name(arg_name):
    default = None

    if arg_name == ParameterNames.DOWN_PAYMENT:
        default = 0.0

    return default
