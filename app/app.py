# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify

from constants import MANDATORY_PAYMENT_AMOUNT_ARGS, MANDATORY_MORTGAGE_PRINCIPAL_ARGS, MORTGAGE_PRINCIPAL_AMOUNT_KEY, \
                      NEW_INTEREST_RATE_KEY, OLD_INTEREST_RATE_KEY, ParameterNames, HttpResponseCodes
from validator import get_argument_dictionary
from mortgage_calculator import calculate_payment_amount_from_asking, calculate_mortgage_principal_amount, \
                                calculate_loan_principal_from_payment
from errors import InvalidArguments, MissingArguments, MissingRequestBody
from db_controller import db, get_db_interest_rate, set_interest_rate, init_interest_rate_if_necessary


app = Flask(__name__)
app.config.from_object('config.ProductionConfig')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


@app.route('/')
def start():
    return "Welcome to Nicole's Mortgage Calculator API."


@app.route('/payment-amount', methods=['GET'])
def get_payment_amount():
    arguments_dict = get_argument_dictionary(request.args, MANDATORY_PAYMENT_AMOUNT_ARGS)

    payment_amount = calculate_payment_amount_from_asking(arguments_dict[ParameterNames.ASKING_PRICE],
                                                          arguments_dict[ParameterNames.DOWN_PAYMENT],
                                                          arguments_dict[ParameterNames.AMORTIZATION_PERIOD],
                                                          arguments_dict[ParameterNames.PAYMENT_SCHEDULE],
                                                          get_db_interest_rate())

    resp = create_json_response({ParameterNames.PAYMENT_AMOUNT: payment_amount})

    return resp


@app.route('/mortgage-amount', methods=['GET'])
def get_mortgage_principle_amount():
    optional_args_dict = {ParameterNames.DOWN_PAYMENT: float}
    arguments_dict = get_argument_dictionary(request.args, MANDATORY_MORTGAGE_PRINCIPAL_ARGS,
                                             optional_args_dict=optional_args_dict)

    loan_principal = calculate_loan_principal_from_payment(arguments_dict[ParameterNames.PAYMENT_AMOUNT],
                                                           arguments_dict[ParameterNames.DOWN_PAYMENT],
                                                           arguments_dict[ParameterNames.AMORTIZATION_PERIOD],
                                                           arguments_dict[ParameterNames.PAYMENT_SCHEDULE],
                                                           get_db_interest_rate())

    mortgage_principal_amount = calculate_mortgage_principal_amount(arguments_dict[ParameterNames.DOWN_PAYMENT],
                                                                    loan_principal)

    resp = create_json_response({MORTGAGE_PRINCIPAL_AMOUNT_KEY: mortgage_principal_amount})

    return resp


@app.route('/interest-rate', methods=['PATCH'])
def patch_interest_rate():
    request_json = request.get_json()

    try:
        new_interest_rate = request_json[ParameterNames.INTEREST_RATE]
        rate_type = type(new_interest_rate)

        if rate_type not in [int, float] or new_interest_rate < 0:
            raise InvalidArguments("%s %s %s" % (ParameterNames.INTEREST_RATE, str(new_interest_rate), rate_type))

    except KeyError:
        raise MissingRequestBody(ParameterNames.INTEREST_RATE)

    old_interest_rate = get_db_interest_rate()
    set_interest_rate(new_interest_rate)

    resp = create_json_response({OLD_INTEREST_RATE_KEY: old_interest_rate,
                                 NEW_INTEREST_RATE_KEY: new_interest_rate})

    return resp


# ERROR HANDLER
@app.errorhandler(InvalidArguments)
@app.errorhandler(MissingArguments)
@app.errorhandler(MissingRequestBody)
def bad_request(error=None):
    message = {
                'status': error.status_code,
                'message': '%s' % (error.message,)
    }

    resp = create_json_response(message, status_code=error.status_code)

    return resp


def create_json_response(response, status_code=HttpResponseCodes.HTTP_OK):
    resp = jsonify(response)
    resp.status_code = status_code

    return resp


if __name__ == '__main__':
    db.app = app
    db.init_app(app)
    db.create_all()

    init_interest_rate_if_necessary()

    app.run()