from constants import NUM_PAYMENTS_BY_SCHEDULE, INSURANCE_VALUES, MAX_INSURABLE_DOWN_PAYMENT_PERCENTAGE, \
                      MAX_INSURABLE_AMOUNT
from errors import InvalidArguments
from utils import round_to_two_decimal_places

"""
    Business logic used in the calculations for payment_amount and mortgage_principal
"""


def calculate_payment_amount_from_asking(asking_price, down_payment, amortization_period, payment_schedule, interest_rate):
    mortgage_insurance = calculate_mortgage_insurance(asking_price, down_payment)
    loan_principal = (asking_price - down_payment) + mortgage_insurance

    num_payments_per_year = NUM_PAYMENTS_BY_SCHEDULE[payment_schedule.lower()]
    total_num_mortgage_payments = num_payments_per_year * amortization_period

    if interest_rate == 0:
        payment_amount = loan_principal / total_num_mortgage_payments
    else:
        interest_rate_per_payment = interest_rate / num_payments_per_year
        total_acc_interest = ((1 + interest_rate_per_payment) ** total_num_mortgage_payments)

        payment_amount = loan_principal * interest_rate_per_payment * total_acc_interest / (total_acc_interest - 1)

    return round_to_two_decimal_places(payment_amount)


# This method is not rounded, because it's currently only used as an intermediate for calculating
# mortgage_principal_amount
def calculate_loan_principal_from_payment(payment_amount, down_payment, amortization_period, payment_schedule, interest_rate):
    num_payments_per_year = NUM_PAYMENTS_BY_SCHEDULE[payment_schedule.lower()]
    total_num_mortgage_payments = num_payments_per_year * amortization_period

    if interest_rate == 0:
        loan_principal = payment_amount * total_num_mortgage_payments
    else:
        interest_rate_per_payment = interest_rate / num_payments_per_year
        total_acc_interest = ((1 + interest_rate_per_payment) ** total_num_mortgage_payments)

        loan_principal = payment_amount * (total_acc_interest - 1) / (interest_rate_per_payment * total_acc_interest)

    return loan_principal


def calculate_mortgage_principal_amount(down_payment, loan_principal):
    temp_vals = INSURANCE_VALUES
    temp_vals.reverse()

    for lower, upper, insurance_rate in temp_vals:
        potential_mortgage_amount = loan_principal / (1 + insurance_rate)
        down_payment_percentage = down_payment / (potential_mortgage_amount + down_payment)

        if lower <= down_payment_percentage < upper:
            return round_to_two_decimal_places(potential_mortgage_amount)

    raise InvalidArguments("Down payment %f is not valid with loan principal value %f" % (down_payment,
                                                                                          loan_principal))


def calculate_mortgage_insurance(asking_price, down_payment):
    insurance_cost = 0
    down_payment_percentage = float(down_payment) / asking_price

    if asking_price <= MAX_INSURABLE_AMOUNT and down_payment_percentage < MAX_INSURABLE_DOWN_PAYMENT_PERCENTAGE:
        insurance_multiplier = _get_insurance_multiplier(down_payment_percentage)
        insurance_cost = insurance_multiplier * (asking_price - down_payment)

    return insurance_cost


def _get_insurance_multiplier(down_payment_percentage):
    insurance_multiplier = 0

    for lower, upper, insurance_rate in INSURANCE_VALUES:
        if lower <= down_payment_percentage < upper:
            insurance_multiplier = insurance_rate

            break

    return insurance_multiplier
